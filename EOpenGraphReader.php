<?php
class EOpenGraphReader extends CWidget{
	public $model = null;
	public $attribute = null;
	public $id = '';
	public $value = '';
	public $controller = 'site';
	public $htmlOptions = array();
	private $type = 0;

	public function init(){
		if($this->model !== null && $this->attribute !== null){
			$this->type = 1;
			$this->id = get_class($this->model).'_'.$this->attribute;
		}

       	$js = dirname(__FILE__).'/assets';
        $jsUrl = Yii::app()->assetManager->publish($js);
		$cs = Yii::app()->clientScript;
		$cs->registerCoreScript('jquery');
		$cs->registerScriptFile($jsUrl.'/EOpenGraphReader.jquery.js', CClientScript::POS_END);
		$cs->registerScript('EOpenGraphReader.'.time(), '
			$("#'.$this->id.'").EOpenGraphReader({
				container: "wrap",
				controller: "'.$this->controller.'"
			});
		', CClientScript::POS_END);
	}

	public function run(){
		if($this->type){
			echo CHtml::activeTextArea($this->model, $this->attribute, $this->htmlOptions);
		}else{
			echo CHtml::textArea($this->id, $this->value, $this->htmlOptions);
		}
	}
}