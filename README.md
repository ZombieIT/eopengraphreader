EOpenGraphReader
================

Yii Framework extension widget для чтения мета данных с web страницы, 
ссылку на которую вы вставляете в textarea своего сайта, аналогично textarea в различных социальных сетях. 

## Подключение
========

####Контроллер
```php
class SiteController extends Controller{
    public function actions(){
        return array(
  		      'EOpenGraphReader' => 'ext.EOpenGraphReader.EOpenGraphReaderAction',
        );
    }
  ...
```

####Виджет и использованием модели
```php
$this->widget('ext.EOpenGraphReader.EOpenGraphReader', array(
    'model' => 'myModel',
    'attribute' => 'myModelAttribute',
));
```

####Виджет без модели
```php
$this->widget('ext.EOpenGraphReader.EOpenGraphReader', array(
    'id' => 'myIdForCreatedTextarea',
));
```