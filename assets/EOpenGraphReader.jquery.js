(function($){
    $.fn.paste = function(listener){
        if ($.browser.msie){
            return this.bind("paste", function (event){
                var that = this;

                setTimeout(function (){
                    listener.apply(that, [event]);
                }, .001);
            });
        }else{
            return this.bind("input", function (event){
                listener.apply(this, [event]);
            });
        }
    };

	$.fn.EOpenGraphReader = function(options) {
		var opts = $.extend({}, $.fn.EOpenGraphReader.defaults, options);
		return this.each(function(){
			$.fn.EOpenGraphReader.init($(this), opts);
	  	});
	};

    $.fn.EOpenGraphReader.init = function(el, opts) {
        var container;
        if(opts.container == 'wrap'){
            container = $('<div id="'+el.attr('id')+'_wrap"/>');
            el.wrap(container);
            $('#'+el.attr('id')+'_wrap').append('<div class="og_place"/>');
        }else{
            container = $(opts.container);
        }

        el.paste(function(e){ 
            var val = el.val();

            if(val != ''){
                $.post('/'+opts.controller+'/EOpenGraphReader', {data: val}, function(responce){                    
                    $('#'+el.attr('id')+'_wrap > .og_place').html(responce);
                });
            }
        });
    };

	//default 
	$.fn.EOpenGraphReader.defaults = {
		container: 'body', //el or wrap
        controller: 'site'
	};
})(jQuery);