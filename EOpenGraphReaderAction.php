<?php
class EOpenGraphReaderAction extends CAction{
    public function  __construct($controller,$id){
        parent::__construct($controller,$id); 
    }

    public function run(){
        $arr = array();
        if(isset($_POST['data'])){
            $data = $_POST['data'];
            require_once(Yii::getPathOfAlias('ext.EOpenGraphReader').'/simple_html_dom.php');

            $reg_exUrl = "/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/";

            $urls = array();
            preg_match_all($reg_exUrl, $data, $urls);
            $isset_urls = array();
            if(isset($urls[0])){
                foreach($urls[0] as $i => $url){
                    $arr = array();
                    if(isset($isset_urls[$url])){
                        continue;
                    }

                    if(preg_match('/(jpg|jpeg|gif|png)$/i', $url)){
                        $arr['image'] = $url;
                         $html = '
                            <div class="og_data">
                                <div class="og_link">
                                    <a href="'.$url.'" target="_blank">'.$url.'</a>
                                </div>
                                <img src="'.$arr['image'].'" align="left" style="margin:0 10px 10px 0;">
                                
                                <input type="hidden" value="'.$arr['image'].'" name="wall-input['.$i.'][og:image]">
                                <input type="hidden" value="'.$url.'" name="wall-input['.$i.'][og:url]">
                                <div style="clear:both;"></div>
                            </div>
                        ';
                        $data = str_replace($url, $html, $data);
                        continue;
                    }

                    $isset_urls[$url] = 1;
                    $html = file_get_html($url);
                    foreach($html->find('meta') as $meta){
                        if($meta->property == 'og:title' && !empty($meta->content)){
                            $arr['title'] = $meta->content;
                        }

                        if($meta->property == 'og:description' && !empty($meta->content)){
                            $arr['description'] = $meta->content;
                        }
                    
                        if((!isset($arr['description']) || empty($arr['description'])) && $meta->property == 'description' && !empty($meta->content)){
                            $arr['description'] = $meta->content;
                        }elseif(
                            (!isset($arr['description']) || empty($arr['description'])) 
                        && $meta->name == 'description' && !empty($meta->content)){
                            $arr['description'] = $meta->content;
                        }elseif(!isset($arr['description'])){
                            $arr['description'] = '';
                        }

                        if($meta->property == 'og:image' && !empty($meta->content)){
                            $arr['image'] = $meta->content;
                        }
                    }

                    $title = $html->find('title');
                    if((!isset($arr['title']) || empty($arr['title'])) && isset($title[0])){
                        $arr['title'] = $title[0];
                    }elseif(!isset($arr['title'])){
                        $arr['title'] = '';
                    }

                    $img = $html->find('img');
                    if((!isset($arr['image']) || empty($arr['image'])) && isset($img[0])){
                        $arr['image'] = $img[0];
                    }elseif(!isset($arr['image'])){
                        $arr['image'] = '';
                    }
                    $html->clear();

                    $arr['description'] = isset($arr['description']) ? (string)$arr['description'] : '';
                    $arr['image'] = isset($arr['image']) ? (string)$arr['image'] : '';
                    $arr['title'] = isset($arr['title']) ? (string)$arr['title'] : '';

                    $html = '
                        <div class="og_data">
                            <div class="og_link">
                                <a href="'.$url.'" target="_blank">'.$url.'</a>
                            </div>
                            '.(!empty($arr['image']) ? ' <img src="'.$arr['image'].'" align="left" style="margin:0 10px 10px 0;">' : '').'
                            <b class="og_title">'.$arr['title'].'</b>
                            <p class="og_description">'.$arr['description'].'</p>
                            <div style="clear:both;"></div>

                            <input type="hidden" value="'.$arr['title'].'" name="wall-input['.$i.'][og:title]">
                            <input type="hidden" value="'.$arr['description'].'" name="wall-input['.$i.'][og:description]">
                            <input type="hidden" value="'.$arr['image'].'" name="wall-input['.$i.'][og:image]">
                            <input type="hidden" value="'.$url.'" name="wall-input['.$i.'][og:url]">
                        </div>
                    ';
                    $data = str_replace($url, $html, $data);
                }
            }
        }

        echo $data;
        Yii::app()->end();
    }
}